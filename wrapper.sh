#!/usr/bin/env bash

OUTPATH="/tmp/strace/$(date +%s%N)/"
mkdir -p "$OUTPATH"

echo "$@" > "$OUTPATH/command.log"

# Run strace as root and output traces at $OUTPATH
#  Limit traces to exec/system syscalls
#  Optionally add -xx to hexa encoding
sudo strace -u "$USER" -ttt -qq -f -ff -X verbose -s 1000 -e signal=SIGKILL -e trace='/(exec|system).*' -o "$OUTPATH/strace" "$@"
