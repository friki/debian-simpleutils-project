#!/usr/bin/env bash

set -e

./update_key_packages_list.sh

wc -l key_packages.list

while read package
do
    echo -n Colecting info from build proces of package: $package ... >> process.log

    sudo git clean . -fd
    rm -rf source
    
    if bash -c "
      ./create_chroot.sh &&
      ./get_source.sh $package &&
      ./wrapper.sh ./build_package.sh $package
    " < /dev/null
    then
        echo Done >> process.log
    else
        echo Error >> process.log
    fi

done < key_packages.list
