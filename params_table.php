<?php

$filename = 'execve-parsed_jq.json';
$json = json_decode(file_get_contents($filename));

$data = [];
foreach ($json as $command => $options) {
    $commandName = basename($command);
    $data[$commandName] = [];

    foreach ($options as $option => $count) {
        if ($option == '--') continue;
        $data[$commandName][$option] = [];
        $data[$commandName][$option]['usage'] = $count>100?'Heavy':($count>10?'Moderate':'Low');
        $data[$commandName][$option]['ieee'] = is_ieee($commandName, $option);
        $data[$commandName][$option]['coreutilsman'] = is_coreutils_man($commandName, $option);
        $data[$commandName][$option]['coreutils'] = is_coreutils($commandName, $option);
        $data[$commandName][$option]['toybox'] = is_toybox($commandName, $option);
        $data[$commandName][$option]['busybox'] = is_busybox($commandName, $option);
        if (!$data[$commandName][$option]['ieee'] &&
            !$data[$commandName][$option]['coreutils'] &&
            !$data[$commandName][$option]['toybox'] &&
            !$data[$commandName][$option]['busybox']) {
            unset($data[$commandName][$option]);
        }
    }
}

echo render_table($data);

function is_ieee(string $commandName, string $option) : bool {
    if (!file_exists("opengroup/$commandName-options.txt")) {
        return false;
    }
    $command = "grep -w -- '$option' opengroup/$commandName-options.txt";
    exec($command, $output, $return_var);
    return $return_var==0;
}

function is_coreutils_man(string $commandName, string $option) : bool {
    $command = "man $commandName | grep -w -- '$option' > /dev/null";
    exec($command, $output, $return_var);
    return $return_var==0;
}

function is_coreutils(string $commandName, string $option) : bool {
    $command = "$commandName --help 2>&1 | grep -w -- '$option' > /dev/null";
    exec($command, $output, $return_var);
    return $return_var==0;
}

function is_toybox(string $commandName, string $option) : bool {
    $command = "toybox $commandName --help | grep -w -- '$option' > /dev/null";
    exec($command, $output, $return_var);
    return $return_var==0;
}

function is_busybox(string $commandName, string $option) : bool {
    $command = "busybox $commandName --help 2>&1 | grep -w -- '$option' > /dev/null";
    exec($command, $output, $return_var);
    return $return_var==0;
}

function render_table(array $data) {
    $html = "<table>\n";

    $html.= "<tr><th>CommandName</th><th>Option</th><th>Usage</th><th>IEEE</th><th>Coreutils Man</th><th>Coreutils --help</th><th>Toybox</th><th>Busybox</th>\n";
    foreach ($data as $commandName => $options) {
        foreach ($options as $option => $info) {
            $usage = $info['usage'];
            $html.= "<tr><td>$commandName</td><td>$option</td><td>$usage</td>";
            foreach (['ieee', 'coreutilsman', 'coreutils', 'toybox', 'busybox'] as $spec) {
                $available = $info[$spec]?'Yes':'No';
                $color = $info[$spec]?'green':'red';
                $html.= "<td bgcolor='$color'>$available</td>";
            }
            $html.= "</tr>\n";
        }
    }
    $html.= "</table>\n";
    return $html;
}
