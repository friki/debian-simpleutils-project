[ -d opengroup ] || mkdir opengroup
cat opengroup_links.list | while read line
do
    command=$(echo $line | awk '{print $1}')
    link=$(echo $line | awk '{print $2}')
    [ -f opengroup/$command.html ] || curl -sL $link > opengroup/$command.html
    lynx --dump opengroup/$command.html > opengroup/$command.txt
    options_pos=$(cat opengroup/$command.txt | grep -n '^    OPTIONS$' | awk -F: '{print $1}')
    operands_pos=$(cat opengroup/$command.txt | grep -n '^    OPERANDS$' | awk -F: '{print $1-1}')
    sed -n "$options_pos,$operands_pos"p opengroup/$command.txt > opengroup/$command-options.txt
done
