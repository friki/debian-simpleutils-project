#!/bin/sh

FILE=key_packages.list

# Happily exit if list already exists
[ -e "$FILE" ] && exit 0

PGPASSWORD=udd-mirror psql --host=udd-mirror.debian.net --user=udd-mirror udd --csv -c '\t' -c 'SELECT * FROM key_packages' -q | \
    awk -F , '{print $1}' > "$FILE"
