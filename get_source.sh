#!/bin/bash

SOURCE=source/
PACKAGE=$1

#gbp clone "https://salsa.debian.org/friki/toybox/" "$SOURCE"
#exit

REPO=$(apt-get source --no-act "$PACKAGE" | grep ^NOTICE -A1| grep 'packaging is maintained in the' -A1 | tail -n1 | awk '{print $1}')

[ -z '$REPO' ] && exit -1

gbp clone "$REPO" "$SOURCE"
