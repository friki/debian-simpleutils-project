#!/usr/bin/env php
<?php

// Usage example:
//   grep execve /tmp/strace -R > /tmp/execve
//   php command_parser.php /tmp/execve > /tmp/execve-parsed.json

if (!array_key_exists(1, $argv)) {
    die("Usage: " . $argv[0] . " <strace.log>");
}

// Commands included in coreutils package
$coreutilsExecs = ["/bin/cat", "/bin/chgrp", "/bin/chmod", "/bin/chown", "/bin/cp",
    "/bin/date", "/bin/dd", "/bin/df", "/bin/dir", "/bin/echo", "/bin/false",
    "/bin/ln", "/bin/ls", "/bin/mkdir", "/bin/mknod", "/bin/mktemp", "/bin/mv",
    "/bin/pwd", "/bin/readlink", "/bin/rm", "/bin/rmdir", "/bin/sleep",
    "/bin/stty", "/bin/sync", "/bin/touch", "/bin/true", "/bin/uname",
    "/bin/vdir", "/usr/bin/[", "/usr/bin/arch", "/usr/bin/b2sum",
    "/usr/bin/base32", "/usr/bin/base64", "/usr/bin/basename",
    "/usr/bin/basenc", "/usr/bin/chcon", "/usr/bin/cksum", "/usr/bin/comm",
    "/usr/bin/csplit", "/usr/bin/cut", "/usr/bin/dircolors",
    "/usr/bin/dirname", "/usr/bin/du", "/usr/bin/env", "/usr/bin/expand",
    "/usr/bin/expr", "/usr/bin/factor", "/usr/bin/fmt", "/usr/bin/fold",
    "/usr/bin/groups", "/usr/bin/head", "/usr/bin/hostid", "/usr/bin/id",
    "/usr/bin/install", "/usr/bin/join", "/usr/bin/link", "/usr/bin/logname",
    "/usr/bin/md5sum", "/usr/bin/md5sum.textutils", "/usr/bin/mkfifo",
    "/usr/bin/nice", "/usr/bin/nl", "/usr/bin/nohup", "/usr/bin/nproc",
    "/usr/bin/numfmt", "/usr/bin/od", "/usr/bin/paste", "/usr/bin/pathchk",
    "/usr/bin/pinky", "/usr/bin/pr", "/usr/bin/printenv", "/usr/bin/printf",
    "/usr/bin/ptx", "/usr/bin/realpath", "/usr/bin/runcon", "/usr/bin/seq",
    "/usr/bin/sha1sum", "/usr/bin/sha224sum", "/usr/bin/sha256sum",
    "/usr/bin/sha384sum", "/usr/bin/sha512sum", "/usr/bin/shred",
    "/usr/bin/shuf", "/usr/bin/sort", "/usr/bin/split", "/usr/bin/stat",
    "/usr/bin/stdbuf", "/usr/bin/sum", "/usr/bin/tac", "/usr/bin/tail",
    "/usr/bin/tee", "/usr/bin/test", "/usr/bin/timeout", "/usr/bin/tr",
    "/usr/bin/truncate", "/usr/bin/tsort", "/usr/bin/tty", "/usr/bin/unexpand",
    "/usr/bin/uniq", "/usr/bin/unlink", "/usr/bin/users", "/usr/bin/wc",
    "/usr/bin/who", "/usr/bin/whoami", "/usr/bin/yes", "/usr/sbin/chroot"];

// Commands included in busybox package
$busyboxExecs = [
    "/bin/arch", "/bin/ash", "/bin/cat", "/bin/chgrp", "/bin/chmod",
    "/bin/chown", "/bin/cp", "/bin/cpio", "/bin/cttyhack", "/bin/date",
    "/bin/dd", "/bin/df", "/bin/dmesg", "/bin/dnsdomainname", "/bin/dumpkmap",
    "/bin/echo", "/bin/egrep", "/bin/false", "/bin/fatattr", "/bin/fgrep",
    "/bin/getopt", "/bin/grep", "/bin/gunzip", "/bin/gzip", "/bin/hostname",
    "/bin/ionice", "/bin/ipcalc", "/bin/kill", "/bin/link", "/bin/linux32",
    "/bin/linux64", "/bin/ln", "/bin/login", "/bin/ls", "/bin/lzop",
    "/bin/mkdir", "/bin/mknod", "/bin/mktemp", "/bin/more", "/bin/mount",
    "/bin/mt", "/bin/mv", "/bin/netstat", "/bin/nuke", "/bin/pidof",
    "/bin/ping", "/bin/ping6", "/bin/ps", "/bin/pwd", "/bin/readlink",
    "/bin/resume", "/bin/rev", "/bin/rm", "/bin/rmdir", "/bin/rpm",
    "/bin/run-parts", "/bin/sed", "/bin/setpriv", "/bin/sh", "/bin/sleep",
    "/bin/stat", "/bin/stty", "/bin/sync", "/bin/tar", "/bin/touch",
    "/bin/true", "/bin/umount", "/bin/uname", "/bin/uncompress", "/bin/usleep",
    "/bin/vi", "/bin/watch", "/bin/zcat", "/linuxrc", "/sbin/acpid",
    "/sbin/adjtimex", "/sbin/arp", "/sbin/blockdev", "/sbin/depmod",
    "/sbin/devmem", "/sbin/freeramdisk", "/sbin/fstrim", "/sbin/getty",
    "/sbin/halt", "/sbin/hwclock", "/sbin/ifconfig", "/sbin/ifdown",
    "/sbin/ifup", "/sbin/init", "/sbin/insmod", "/sbin/ip", "/sbin/ipneigh",
    "/sbin/klogd", "/sbin/loadkmap", "/sbin/logread", "/sbin/losetup",
    "/sbin/lsmod", "/sbin/mdev", "/sbin/mkdosfs", "/sbin/mke2fs",
    "/sbin/mkswap", "/sbin/modinfo", "/sbin/modprobe", "/sbin/nameif",
    "/sbin/pivot_root", "/sbin/poweroff", "/sbin/reboot", "/sbin/rmmod",
    "/sbin/route", "/sbin/run-init", "/sbin/start-stop-daemon",
    "/sbin/swapoff", "/sbin/swapon", "/sbin/switch_root", "/sbin/sysctl",
    "/sbin/syslogd", "/sbin/udhcpc", "/sbin/uevent", "/sbin/vconfig",
    "/sbin/watchdog", "/usr/bin/[", "/usr/bin/[[", "/usr/bin/ar",
    "/usr/bin/awk", "/usr/bin/basename", "/usr/bin/bc", "/usr/bin/blkdiscard",
    "/usr/bin/bunzip2", "/usr/bin/bzcat", "/usr/bin/bzip2", "/usr/bin/cal",
    "/usr/bin/chvt", "/usr/bin/clear", "/usr/bin/cmp", "/usr/bin/cut",
    "/usr/bin/dc", "/usr/bin/deallocvt", "/usr/bin/diff", "/usr/bin/dirname",
    "/usr/bin/dos2unix", "/usr/bin/du", "/usr/bin/dumpleases", "/usr/bin/env",
    "/usr/bin/expand", "/usr/bin/expr", "/usr/bin/factor",
    "/usr/bin/fallocate", "/usr/bin/find", "/usr/bin/fold", "/usr/bin/free",
    "/usr/bin/ftpget", "/usr/bin/ftpput", "/usr/bin/groups", "/usr/bin/head",
    "/usr/bin/hexdump", "/usr/bin/hostid", "/usr/bin/id", "/usr/bin/killall",
    "/usr/bin/last", "/usr/bin/less", "/usr/bin/logger", "/usr/bin/logname",
    "/usr/bin/lsscsi", "/usr/bin/lzcat", "/usr/bin/lzma", "/usr/bin/md5sum",
    "/usr/bin/microcom", "/usr/bin/mkfifo", "/usr/bin/mkpasswd", "/usr/bin/nc",
    "/usr/bin/nl", "/usr/bin/nproc", "/usr/bin/nsenter", "/usr/bin/nslookup",
    "/usr/bin/od", "/usr/bin/openvt", "/usr/bin/paste", "/usr/bin/patch",
    "/usr/bin/printf", "/usr/bin/realpath", "/usr/bin/renice",
    "/usr/bin/reset", "/usr/bin/rpm2cpio", "/usr/bin/seq",
    "/usr/bin/setkeycodes", "/usr/bin/setsid", "/usr/bin/sha1sum",
    "/usr/bin/sha256sum", "/usr/bin/sha512sum", "/usr/bin/shred",
    "/usr/bin/shuf", "/usr/bin/sort", "/usr/bin/ssl_client",
    "/usr/bin/strings", "/usr/bin/svc", "/usr/bin/svok", "/usr/bin/tac",
    "/usr/bin/tail", "/usr/bin/taskset", "/usr/bin/tee", "/usr/bin/telnet",
    "/usr/bin/test", "/usr/bin/tftp", "/usr/bin/time", "/usr/bin/timeout",
    "/usr/bin/top", "/usr/bin/tr", "/usr/bin/traceroute",
    "/usr/bin/traceroute6", "/usr/bin/truncate", "/usr/bin/tty",
    "/usr/bin/unexpand", "/usr/bin/uniq", "/usr/bin/unix2dos",
    "/usr/bin/unlink", "/usr/bin/unlzma", "/usr/bin/unshare", "/usr/bin/unxz",
    "/usr/bin/unzip", "/usr/bin/uptime", "/usr/bin/uudecode",
    "/usr/bin/uuencode", "/usr/bin/w", "/usr/bin/wc", "/usr/bin/wget",
    "/usr/bin/which", "/usr/bin/who", "/usr/bin/whoami", "/usr/bin/xargs",
    "/usr/bin/xxd", "/usr/bin/xz", "/usr/bin/xzcat", "/usr/bin/yes",
    "/usr/sbin/arping", "/usr/sbin/brctl", "/usr/sbin/chroot",
    "/usr/sbin/fsfreeze", "/usr/sbin/httpd", "/usr/sbin/i2cdetect",
    "/usr/sbin/i2cdump", "/usr/sbin/i2cget", "/usr/sbin/i2cset",
    "/usr/sbin/loadfont", "/usr/sbin/nologin", "/usr/sbin/partprobe",
    "/usr/sbin/rdate", "/usr/sbin/ubirename", "/usr/sbin/udhcpd"
];

// Commands included in toybox package
$toyboxExecs = [
    //"/bin/bash",
    "/bin/blkdiscard", "/bin/blkid", "/bin/cat", "/bin/chattr", "/bin/chgrp",
    "/bin/chmod", "/bin/chown", "/bin/cksum", "/bin/cp", "/bin/cpio",
    "/bin/crc32", "/bin/date", "/bin/demo_many_options", "/bin/demo_number",
    "/bin/demo_scankey", "/bin/dmesg", "/bin/dnsdomainname", "/bin/dos2unix",
    "/bin/echo", "/bin/egrep", "/bin/false", "/bin/fgrep", "/bin/fstype",
    "/bin/fsync", "/bin/grep", "/bin/help", "/bin/hostname", "/bin/kill",
    "/bin/last", "/bin/ln", "/bin/login", "/bin/ls", "/bin/lsattr",
    "/bin/mkdir", "/bin/mknod", "/bin/mktemp", "/bin/mount", "/bin/mountpoint",
    "/bin/mv", "/bin/netcat", "/bin/netstat", "/bin/nice", "/bin/pidof",
    "/bin/printenv", "/bin/ps", "/bin/pwd", "/bin/readahead", "/bin/rm",
    "/bin/rmdir", "/bin/route", "/bin/sed", "/bin/sh", "/bin/sleep",
    "/bin/stat", "/bin/stty", "/bin/su", "/bin/sync", "/bin/telnet",
    "/bin/tftpd", "/bin/touch", "/bin/toysh", "/bin/true", "/bin/umount",
    "/bin/uname", "/bin/unix2dos", "/bin/usleep", "/bin/vmstat",
    "/sbin/addgroup", "/sbin/adduser", "/sbin/blockdev", "/sbin/delgroup",
    "/sbin/deluser", "/sbin/df", "/sbin/dhcp", "/sbin/dhcp6", "/sbin/dhcpd",
    "/sbin/fdisk", "/sbin/freeramdisk", "/sbin/getty", "/sbin/groupadd",
    "/sbin/groupdel", "/sbin/halt", "/sbin/hwclock", "/sbin/ifconfig",
    "/sbin/init", "/sbin/insmod", "/sbin/ip", "/sbin/ipaddr", "/sbin/iplink",
    "/sbin/iproute", "/sbin/iprule", "/sbin/iptunnel", "/sbin/killall5",
    "/sbin/klogd", "/sbin/losetup", "/sbin/lsmod", "/sbin/mke2fs",
    "/sbin/mkswap", "/sbin/modinfo", "/sbin/modprobe", "/sbin/oneit",
    "/sbin/partprobe", "/sbin/pivot_root", "/sbin/poweroff", "/sbin/reboot",
    "/sbin/rmmod", "/sbin/sulogin", "/sbin/swapoff", "/sbin/swapon",
    "/sbin/switch_root", "/sbin/sysctl", "/sbin/syslogd", "/sbin/useradd",
    "/sbin/userdel", "/sbin/vconfig", "/usr/bin/acpi", "/usr/bin/arch",
    "/usr/bin/arp", "/usr/bin/ascii", "/usr/bin/base64", "/usr/bin/basename",
    "/usr/bin/bc", "/usr/bin/bunzip2", "/usr/bin/bzcat", "/usr/bin/cal",
    "/usr/bin/catv", "/usr/bin/chrt", "/usr/bin/chvt", "/usr/bin/clear",
    "/usr/bin/cmp", "/usr/bin/comm", "/usr/bin/count", "/usr/bin/cut",
    "/usr/bin/dd", "/usr/bin/deallocvt", "/usr/bin/demo_utf8towc",
    "/usr/bin/devmem", "/usr/bin/diff", "/usr/bin/dirname", "/usr/bin/du",
    "/usr/bin/dumpleases", "/usr/bin/eject", "/usr/bin/env", "/usr/bin/expand",
    "/usr/bin/expr", "/usr/bin/factor", "/usr/bin/fallocate", "/usr/bin/file",
    "/usr/bin/find", "/usr/bin/flock", "/usr/bin/fmt", "/usr/bin/fold",
    "/usr/bin/free", "/usr/bin/fsck", "/usr/bin/ftpget", "/usr/bin/ftpput",
    "/usr/bin/getconf", "/usr/bin/getfattr", "/usr/bin/getopt",
    "/usr/bin/groups", "/usr/bin/gunzip", "/usr/bin/gzip", "/usr/bin/head",
    "/usr/bin/hello", "/usr/bin/hexedit", "/usr/bin/host", "/usr/bin/hostid",
    "/usr/bin/i2cdetect", "/usr/bin/i2cdump", "/usr/bin/i2cget",
    "/usr/bin/i2cset", "/usr/bin/iconv", "/usr/bin/id", "/usr/bin/inotifyd",
    "/usr/bin/install", "/usr/bin/ionice", "/usr/bin/iorenice",
    "/usr/bin/iotop", "/usr/bin/ipcrm", "/usr/bin/ipcs", "/usr/bin/killall",
    "/usr/bin/link", "/usr/bin/logger", "/usr/bin/logname",
    "/usr/bin/logwrapper", "/usr/bin/lsof", "/usr/bin/lspci", "/usr/bin/lsusb",
    "/usr/bin/makedevs", "/usr/bin/man", "/usr/bin/mcookie", "/usr/bin/md5sum",
    "/usr/bin/mdev", "/usr/bin/microcom", "/usr/bin/mix", "/usr/bin/mkfifo",
    "/usr/bin/mkpasswd", "/usr/bin/more", "/usr/bin/nc", "/usr/bin/nl",
    "/usr/bin/nohup", "/usr/bin/nproc", "/usr/bin/nsenter", "/usr/bin/od",
    "/usr/bin/passwd", "/usr/bin/paste", "/usr/bin/patch", "/usr/bin/pgrep",
    "/usr/bin/ping", "/usr/bin/ping6", "/usr/bin/pkill", "/usr/bin/pmap",
    "/usr/bin/printf", "/usr/bin/prlimit", "/usr/bin/pwdx", "/usr/bin/readelf",
    "/usr/bin/readlink", "/usr/bin/realpath", "/usr/bin/renice",
    "/usr/bin/reset", "/usr/bin/rev", "/usr/bin/rtcwake", "/usr/bin/seq",
    "/usr/bin/setfattr", "/usr/bin/setsid", "/usr/bin/sha1sum",
    "/usr/bin/sha224sum", "/usr/bin/sha256sum", "/usr/bin/sha384sum",
    "/usr/bin/sha512sum", "/usr/bin/shred", "/usr/bin/skeleton",
    "/usr/bin/skeleton_alias", "/usr/bin/sntp", "/usr/bin/sort",
    "/usr/bin/split", "/usr/bin/strings", "/usr/bin/tac", "/usr/bin/tail",
    "/usr/bin/tar", "/usr/bin/taskset", "/usr/bin/tee", "/usr/bin/telnetd",
    "/usr/bin/test", "/usr/bin/tftp", "/usr/bin/time", "/usr/bin/timeout",
    "/usr/bin/top", "/usr/bin/tr", "/usr/bin/traceroute",
    "/usr/bin/traceroute6", "/usr/bin/truncate", "/usr/bin/tty",
    "/usr/bin/tunctl", "/usr/bin/ulimit", "/usr/bin/uniq", "/usr/bin/unlink",
    "/usr/bin/unshare", "/usr/bin/uptime", "/usr/bin/uudecode",
    "/usr/bin/uuencode", "/usr/bin/uuidgen", "/usr/bin/vi", "/usr/bin/w",
    "/usr/bin/watch", "/usr/bin/wc", "/usr/bin/wget", "/usr/bin/which",
    "/usr/bin/who", "/usr/bin/whoami", "/usr/bin/xargs", "/usr/bin/xxd",
    "/usr/bin/xzcat", "/usr/bin/yes", "/usr/bin/zcat", "/usr/sbin/arping",
    "/usr/sbin/brctl", "/usr/sbin/chroot", "/usr/sbin/crond",
    "/usr/sbin/fsfreeze", "/usr/sbin/rfkill"
];

$execs = array_merge($coreutilsExecs, $busyboxExecs, $toyboxExecs);

$log = file($argv[1], FILE_IGNORE_NEW_LINES);

$commandArgs = [];
foreach ($log as $line) {
    preg_match_all('@execve\("(.*)\)\ =\ @', $line, $matches);
    $execveParams = $matches[1][0];
    //var_dump($execveParams);

    // Get command pathname
    $endFirst = strpos($execveParams, '", ["');
    $execName = substr($execveParams, 0, $endFirst);
    //var_dump($execName);
    $execveParamsTobeparsed = substr($execveParams, $endFirst + 5);

    // Get command argv
    $endArgv = strrpos($execveParamsTobeparsed, '"], 0x');
    $execArgv = substr($execveParamsTobeparsed, 0, $endArgv);
    //var_dump($execArgv);
    $parsedExecArgv = explode('", "', $execArgv);
    //var_dump($parsedExecArgv);
    $execveParamsTobeparsed = substr($execveParamsTobeparsed, $endArgv + 4);

    // Get envp
    $execEnvp = $execveParamsTobeparsed;


    // Params analysis
    //   Started by '-' and limited by '='
    //   TODO: Lot of work here
    if (in_array($execName, $execs)) {
        if (!array_key_exists($execName, $commandArgs)) {
            $commandArgs[$execName] = [];
        }
        foreach ($parsedExecArgv as $arg) {
            if (strlen($arg) && $arg[0] == '-') {
                $end = strpos($arg, '=');
                if ($end===false) {
                    $parsedArg = $arg;
                } else {
                    $parsedArg = substr($arg, 0, $end);
                }

                if (!array_key_exists($parsedArg, $commandArgs[$execName])) {
                    $commandArgs[$execName][$parsedArg] = 0;
                }
                $commandArgs[$execName][$parsedArg]++;
            }
        }
        ksort($commandArgs[$execName]);
    }
}

ksort($commandArgs);
echo json_encode($commandArgs);
