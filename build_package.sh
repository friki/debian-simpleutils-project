#!/bin/bash

# # Abort if no args[1]
# [ -z "$1" ] && exit -1
# 
# PACKAGE=$1
SOURCE=source/
CHROOT=chroot/

[ ! -d "$SOURCE" ] && \
    echo "Please \"gbp clone\" $PACKAGE source and place at \"$SOURCE\"" && \
    exit -1

BASE="$PWD"
(pushd "$SOURCE" && 
    gbp buildpackage --git-ignore-branch --git-pbuilder --git-ignore-new --git-pbuilder-options="--buildresult \"$BASE/buildresult\" --basepath \"$BASE\"/\"$CHROOT\"")
